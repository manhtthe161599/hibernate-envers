package com.example.excel.com;

import jakarta.persistence.*;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String Name;
    @Column(name = "description")
    private String Description;
    @Column(name = "priority")
    private String Priority;
    @Column(name = "pic")
    private String Pic;
    @Column(name = "owner")
    private String Owner;
    @CreatedBy
    @Column(name = "createdby")
    private String CreatedBy;
    @CreatedDate
    @Column(name = "createddate")
    private LocalDate CreatedDate;
    @LastModifiedDate
    @Column(name = "updateddate")
    private LocalDate UpdatedDate;
    @LastModifiedBy
    @Column(name = "updatedby")
    private String UpdatedBy;

    public Task() {
    }



    public Task(int id, String name, String description, String priority, String pic, String owner, String createdBy, LocalDate createdDate, LocalDate updatedDate, String updatedBy) {
        this.id = id;
        this.Name = name;
        this.Description = description;
        this.Priority = priority;
        this.Pic = pic;
        this.Owner = owner;
        this.CreatedBy = createdBy;
        this.CreatedDate = createdDate;
        this.UpdatedDate = updatedDate;
        this.UpdatedBy = updatedBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        this.Priority = priority;
    }

    public String getPic() {
        return Pic;
    }

    public void setPic(String pic) {
        this.Pic = pic;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        this.Owner = owner;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        this.CreatedBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.CreatedDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.UpdatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.UpdatedBy = updatedBy;
    }

    @PrePersist
    protected void onCreate() {
        CreatedBy = "manhtt";
        UpdatedBy = "manhtt";
    }

    @PreUpdate
    protected void onUpdate() {
        UpdatedBy = "manhtt";
    }
}
