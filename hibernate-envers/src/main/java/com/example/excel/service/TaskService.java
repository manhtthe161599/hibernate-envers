//package com.example.excel.service;
//
//import com.example.excel.com.Task;
//import com.example.excel.repository.TaskRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class TaskService {
//    private final TaskRepository taskRepository;
//
//
//    @Autowired
//    public TaskService(TaskRepository taskRepository) {
//        this.taskRepository = taskRepository;
//    }
//
//    public List<Task> getAllTasks() {
//        return taskRepository.findAll();
//    }
//
//
//    public Task createTask(Task task) {
//        return taskRepository.save(task);
//    }
//
//    public Task updateTask(int id, Task updatedTask) {
//        Task existingTask = taskRepository.findById(id).orElse(null);
//        if (existingTask == null) {
//            return null;
//        }
//        // Cập nhật thông tin Task
//        existingTask.setName(updatedTask.getName());
//        existingTask.setDescription(updatedTask.getDescription());
//        // ... Cập nhật các thuộc tính khác
//        return taskRepository.save(existingTask);
//    }
//
//
//}
//



package com.example.excel.service;
import com.example.excel.com.Task;
import com.example.excel.com.TaskHistory;
import com.example.excel.repository.TaskRepository;
import com.example.excel.repository.TaskHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;


@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final TaskHistoryRepository taskHistoryRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, TaskHistoryRepository taskHistoryRepository) {
        this.taskRepository = taskRepository;
        this.taskHistoryRepository = taskHistoryRepository;
    }

    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }


    public Task createTask(Task task) {
        Task createdTask = taskRepository.save(task);

        // Tạo một bản sao của Task và sao chép dữ liệu vào TaskHistory
        TaskHistory taskHistory = new TaskHistory();
        copyTaskToTaskHistory(createdTask, taskHistory);
        taskHistory.setTaskId(createdTask.getId());
        taskHistory.setAction("add");

        // Lưu TaskHistory vào bảng task_history
        taskHistoryRepository.save(taskHistory);
        return createdTask;
    }

    public Task updateTask(int id, Task updatedTask) {
        Task existingTask = taskRepository.findById(id).orElse(null);
        if (existingTask == null) {
            return null;
        }
        // update Task
        existingTask.setName(updatedTask.getName());
        existingTask.setDescription(updatedTask.getDescription());
        Task updatedTaskResult = taskRepository.save(existingTask);

        // Tạo một bản sao của Task và cho vào TaskHistory
        TaskHistory taskHistory = new TaskHistory();
        copyTaskToTaskHistory(updatedTaskResult, taskHistory);
        taskHistory.setTaskId(updatedTaskResult.getId());
        taskHistory.setAction("update");

        // Lưu vào bảng task_history
        taskHistoryRepository.save(taskHistory);

        return updatedTaskResult;
    }


    private void copyTaskToTaskHistory(Task source, TaskHistory destination) {
        destination.setName(source.getName());
        destination.setDescription(source.getDescription());
        destination.setPriority(source.getPriority());
        destination.setPic(source.getPic());
        destination.setOwner(source.getOwner());
        destination.setCreatedBy(source.getCreatedBy());
        destination.setCreatedDate(source.getCreatedDate());
        destination.setUpdatedDate(source.getUpdatedDate());
        destination.setUpdatedBy(source.getUpdatedBy());
    }
}

